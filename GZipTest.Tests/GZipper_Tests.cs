﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Threading;
using static GZipper.Tests.TestData;
using static GZipper.StateBase;
using static GZipper.ProcessManager;

namespace GZipper.Tests
{
    [TestClass]
    public class GZipper_Tests          //check bytes count on processed files
    {
        public void ExecuteProcess (IProcessManager manager, CheckFileInfo info, ProcessMode mode)
        {
            //arrange
            ProcessInitData initData = new ProcessInitData(info.GetSourceFile(mode), info.GetTargetFile(mode), mode);
            
            //act
            if (initData.TargetFile.Exists)
                initData.TargetFile.Delete();

            manager.StartProcess(initData, null);

            //assert
            FileInfo trgFile = info.GetTargetFile(mode);
            Assert.AreEqual(info.GetTargetSize(mode), trgFile.Length);
        }

        [TestMethod]
        public void CompressInOneThread_BytesCompare ()
        {
            foreach (CheckFileInfo info in TestFiles)
                ExecuteProcess(new OneThreadManager(), info, ProcessMode.Compress);
        }

        [TestMethod]
        public void CompressInMultipleThreads_BytesCompare ()
        {
            foreach (CheckFileInfo info in TestFiles)
                ExecuteProcess(new MultiThreadManager(), info, ProcessMode.Compress);
        }

        [TestMethod]
        public void DecompressInOneThread_BytesCompare ()
        {
            foreach (CheckFileInfo info in TestFiles)
                ExecuteProcess(new OneThreadManager(), info, ProcessMode.Decompress);
        }

        [TestMethod]
        public void DecompressInMultipleThreads_BytesCompare ()
        {
            foreach (CheckFileInfo info in TestFiles)
                ExecuteProcess(new MultiThreadManager(), info, ProcessMode.Decompress);
        }
    }
}
