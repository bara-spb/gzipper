﻿using System.Collections.Generic;
using System.IO;
using static GZipper.StateBase;

namespace GZipper.Tests
{
    public struct CheckFileInfo
    {
        public string OriginName;
        public string CompressedName;
        public string DecompressedName { get {
                return string.Format("{0}\\{1}_decompress{2}", Path.GetDirectoryName(OriginName), Path.GetFileNameWithoutExtension(OriginName), Path.GetExtension(OriginName));
            } }
        public long OriginSize;
        public long CompressedSize;

        public CheckFileInfo (string file, long origin, long compressed)
        {
            OriginName = "..\\..\\..\\testDir\\" + file;
            CompressedName = "..\\..\\..\\testDir\\" + file + ".gz";
            OriginSize = origin;
            CompressedSize = compressed;
        }
        public long GetTargetSize(ProcessMode mode)
        {
            return mode == ProcessMode.Compress ? CompressedSize : OriginSize;
        }

        public long GetSourceSize (ProcessMode mode)
        {
            return mode == ProcessMode.Compress ? OriginSize : CompressedSize;
        }

        public FileInfo GetSourceFile (ProcessMode mode)
        {
            string name = mode == ProcessMode.Compress ? OriginName : CompressedName;
            return new FileInfo(name);
        }
        public FileInfo GetTargetFile (ProcessMode mode)
        {
            string name = mode == ProcessMode.Compress ? CompressedName : DecompressedName;
            return new FileInfo(name);
        }
    }

    public static class TestData
    {
        public static List<CheckFileInfo> TestFiles = new List<CheckFileInfo>
        {
            new CheckFileInfo("pic.png", 141097, 202091),
            new CheckFileInfo("doc.pdf", 1140748, 888009),
            new CheckFileInfo("vid.mp4", 72481804, 111274153)
        };

        public static CheckFileInfo GetTestFileInfo(string name)
        {
            foreach (CheckFileInfo file in TestFiles)
            {
                if (file.OriginName == "..\\..\\..\\testDir\\" + name)
                    return file;
            }
            throw new KeyNotFoundException(string.Format("There is no {0} in test files", name));
        }
    }
}
