﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using static GZipper.ProcessManager;
using static GZipper.StateBase;
using static GZipper.Utils.GZipUtils;

namespace GZipper
{
    public partial class GZipper
    {
        public const string cmdCompress = "compress";
        public const string cmdDecompress = "decompress";

        IProcessManager _pManager;
                
        public GZipper ()
        {
            if (Environment.ProcessorCount == 1)
                _pManager = new OneThreadManager();
            else
                _pManager = new MultiThreadManager();
        }

        //Выполнить команду
        internal bool Execute (params string[] args)
        {
            if (args.Length < 2 || args.Length > 3)
                return PrintErrorMessage("Wrong arguments count.");

            string command = args[0];
            string src = args[1];
            string trg = args.Length == 3 ? args[2] : string.Empty;

            FileInfo srcFile, trgFile;
                        
            if (!CheckArgs(command, src, trg, out srcFile, out trgFile))    //проверка аргументов
                return false;

            if (!DeleteTargetFile(trgFile))
                return false;


            Console.WriteLine("{0} of {1} started...", (command == cmdCompress ? "Compression" : "Decompression"), srcFile.Name);

            Stopwatch watch = new Stopwatch();
            watch.Start();

            ProcessInitData initData = new ProcessInitData(srcFile, trgFile, command);
            ProcessResult result = _pManager.StartProcess(initData, ShowProgress);

            watch.Stop();

            if (result.Success)
            {
                Console.SetCursorPosition(0, Console.CursorTop);
                Console.WriteLine("Progress............100%");
                FileInfo dest = new FileInfo(trgFile.FullName);
                Console.WriteLine("{4} to {0} from {1} to {2} bytes completed in {3} ms.",
                    dest.Name, srcFile.Length.ToString(), dest.Length.ToString(), watch.ElapsedMilliseconds, (command == cmdCompress ? "Compressed" : "Decompressed"));
                return true;
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Process was interrupted!");
                Console.WriteLine(result.ErrorMsg);
                return false;
            }
        }

        //Отображение прогресса выполнения процесса обработки
        protected void ShowProgress(double progress)
        {
            int persent = 0;
            if (progress > persent)
                persent = (int) Math.Min(progress * 100, 99);
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write("Progress............{0:0}%", persent);
        }

        //Удаление файла перед записью новых данных
        private bool DeleteTargetFile(FileInfo trgFile)
        {
            if (trgFile.Exists)
            {
                try
                {
                    trgFile.Delete();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed to acces {0}", trgFile.Name);
                    Console.WriteLine(ex.Message);
                    return false;
                }
            }
            return true;
        }

        //Проверка аргументов
        private bool CheckArgs(string cmd, string src, string trg, out FileInfo srcInfo, out FileInfo trgInfo)
        {
            srcInfo = new FileInfo(src);
            trgInfo = trg != String.Empty ? new FileInfo(trg) : null;

            if(cmd != cmdCompress && cmd != cmdDecompress)
                return PrintErrorMessage("Unknown command.");

            //Проверка существования исходного файла
            if (!srcInfo.Exists)
                return PrintErrorMessage(String.Format("Can't find \"{0}\".", srcInfo.FullName));
            
            //При сжатии
            if (cmd == cmdCompress)
            {
                //Исходный файл не должен быть архивом .gz
                if (srcInfo.Extension == ".gz")
                    return PrintErrorMessage("Source file shouldn't be a compressed type!");

                //Если выходной файл не указан
                if (trg == String.Empty)
                    trgInfo = new FileInfo(src + ".gz");                

                //Если путь к выходному файлу не указывает на существующую директорию
                else if (Path.GetDirectoryName(trg) != String.Empty && !Directory.Exists(Path.GetDirectoryName(trg)))
                    return PrintErrorMessage("Target file directory not exists!");

                //Если выходной файл указан без расширения .gz
                else if (Path.GetExtension(trg) != (".gz"))
                    trgInfo = new FileInfo(String.Concat(trg, ".gz"));

                return true;
            }
            //При декомпрессии
            else
            {
                //Исходный файл должен быть архивом .gz
                if (srcInfo.Extension != ".gz")
                    return PrintErrorMessage("Source file sould have .gz extension!");

                //Если выходной файл не указан
                if (trg == String.Empty)
                    trgInfo = new FileInfo(srcInfo.Directory + "\\" + Path.GetFileNameWithoutExtension(src));

                //Если путь к выходному файлу не указывает на существующую директорию
                else if (Path.GetDirectoryName(trg) != String.Empty && !Directory.Exists(Path.GetDirectoryName(trg)))
                    return PrintErrorMessage("Target file directory not exists!");

                //TODO delete - add "_decompression" suffix
                //trg = trgInfo.DirectoryName + "\\" + Path.GetFileNameWithoutExtension(trgInfo.FullName) + "_decompress" + trgInfo.Extension;
                //trgInfo = new FileInfo(trg);

                return true;
            }
        }

        //Вывод сообщения об ошибке
        private bool PrintErrorMessage (string msg)
        {
            Console.WriteLine(msg);
            Console.WriteLine(String.Format("usage: gziptest {0}|{1} <sourceFile> [<targetFile>]", cmdCompress, cmdDecompress));
            Console.WriteLine("Note, that if your file path contains spaces, path should be placed in quotes.");
            return false;
        }


    }
}
