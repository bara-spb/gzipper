﻿using System;
using System.Threading;

namespace GZipper
{
    public interface IProcessManager
    {
        ProcessResult StartProcess (ProcessInitData initData, Action<double> progressCallback);     //запуск процесса обработки
    }

}
