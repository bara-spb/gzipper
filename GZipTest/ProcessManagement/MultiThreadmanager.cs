﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;

namespace GZipper
{
    public sealed class MultiThreadManager : ProcessManager
    {
        public enum ThreadFunc { NONE, READ, ZIP, WRITE }       //назначения потоков

        private int _waitTime = 300;        //время ожидания потока до повторной попытки записи при нехватке памяти

        public MultiThreadManager () : base() { }

        //Запуск процесса обработки
        public override ProcessResult StartProcess (ProcessInitData initData, Action<double> progressCallback)
        {
            using (MultiThreadState state = new MultiThreadState(initData))
            {
                List<Thread> threads = new List<Thread>();
                try
                {
                    threads.Add(new Thread(() => ReadFromFile(state)));
                    threads.Add(new Thread(() => WriteToFile(state)));
                    for (int i = 0; i < Environment.ProcessorCount; i++)
                        threads.Add(new Thread(() => ZipToBuffer(state)));

                    threads.ToList().ForEach(j => j.Start());      //запуск потоков исполнения

                    while (state.State == ProcessStates.InWork)     //ожидание завершения процесса обработки
                    {
                        progressCallback?.Invoke(state.Progress);   //отображение прогресса выполнения
                        Thread.Sleep(200);
                    }

                }
                catch (Exception ex)
                {
                    state.SetError(ex.Message);     //ошибка выполнения
                }
                finally
                {
                    threads.ToList().ForEach(t => t?.Abort());      //завершение всех потоков
                }

                return new ProcessResult(state.State == ProcessStates.Succeed, state.ErrorMsg);
            }
        }

        //Чтение файлапо частям и их сохранение в промежуточное хранилище (srcFile -> rQueue)
        public void ReadFromFile (MultiThreadState state)
        {
            using (FileStream fromStream = new FileStream(state.srcFile.FullName, FileMode.Open, FileAccess.Read, FileShare.Read, state.BlockSize))
            {
                int size = state.PartLength;
                if (state.Mode == ProcessMode.Decompress)
                    size += 2 * state.BlockSize;   //при декомперссии увеличиваем размер считываемого буфера для нахождения последнего блока группы, выходящего за ее границы
                byte[] buffer = new byte[size];
                int indx = 0;

                while (state.GetNextReadIndx(ref indx))      //получаем индекс группы для чтения
                {
                    if(state.Mode == ProcessMode.Decompress)
                        fromStream.Position = (long)  indx * state.PartLength;

                    int bytesRead = fromStream.Read(buffer, 0, size);     //считываем данные

                    if (bytesRead != size)
                    {
                        if (fromStream.Position == fromStream.Length)   //при достижении конца файла
                        {
                            while (!state.AddReadPart(indx, buffer.Take(bytesRead).ToArray()))     //добавляем последнюю группу
                                state.ReadThreadNeed.WaitOne();

                            if (indx == state.TotalParts - 1)  //при декомпрессии группы читаются с перехлестом - завершаем чтение только при достижении последней группы
                                break;
                            else
                                continue;
                        }
                        else
                        {
                            state.SetError("Error occured while reading the file!");   //ошибка чтения
                            return;
                        }
                    }

                    while (!state.AddReadPart(indx, buffer.ToArray()))     //добавляем считанную группу в очередь на сжатие
                        state.ReadThreadNeed.WaitOne(_waitTime);     //если недостаточно памяти  - прекращаем чтение до запроса на новую группу блоков
                }
            }
        }


        //Сжатие блоков и сохранение в промежуточное хранилище (rQueue -> zQueue)
        public void ZipToBuffer (MultiThreadState state)
        {
            BlockPart rPart;
            List<byte> zPart;
            byte[] block = new byte[state.BlockSize];

            while (!state.isAllDataHandle(ThreadFunc.ZIP))
            {
                state.ZipThreadNeed.WaitOne();     //ожидаем считанную группу

                while (state.GetReadPart(out rPart))     //получаем следующую считанную группу
                {
                    if (state.Mode == ProcessMode.Compress)
                        Zip(state, out zPart, ref rPart);
                    else
                        Unzip(state, out zPart, ref rPart);                    

                    while (!state.AddZipPart(rPart.indx, zPart.ToArray()))     //добвляем группу сжатых блоков в очередь на запись
                        state.ZipThreadNeed.WaitOne(_waitTime);     //недостаточно памяти для записи - прекращаем обработку до запроса на новую группу блоков
                }
            }
        }

        //Сжатие блоков и сохранение в промежуточное хранилище (rQueue -> zQueue)
        private void Zip (MultiThreadState state, out List<byte> zPart, ref BlockPart rPart)
        {
            zPart = new List<byte>();
            byte[] block = new byte[state.BlockSize];
            int blockCount = (int) Math.Ceiling((double) rPart.array.Length / state.BlockSize);
            int blockSize;
            
            for (int i = 0; i < blockCount; i++)
            {
                blockSize = i == blockCount - 1 ? rPart.array.Length - i * state.BlockSize : block.Length;
                Array.Copy(rPart.array, i * state.BlockSize, block, 0, blockSize);    //выделяем блок для сжатия

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (GZipStream gzipStream = new GZipStream(mStream, CompressionMode.Compress, true))
                    {
                        gzipStream.Write(block, 0, blockSize);   //сжимаем блок
                    }

                    zPart.AddRange(mStream.ToArray());      //записываем сжатый блок в буффер группы
                }
            }
        }

        //Распаковка блоков и сохранение в промежуточное хранилище (rQueue -> zQueue)
        private void Unzip (MultiThreadState state, out List<byte> zPart, ref BlockPart rPart)
        {
            zPart = new List<byte>();
            List<byte> block = new List<byte>();
            bool isPrefixFound;

            List<byte> prefixBuffer = new List<byte>();  //структура для поиска начала блока
            
            isPrefixFound = false;

            if (rPart.indx == 0)   //для первого сектора проверяем наличие заголовка   
            {
                if (!_gzip.IsGZipPrefix(rPart.array.Take(_gzip.Prefix.Length).ToArray()))    //если начало не соответствует заголовку
                {
                    state.SetError("Gzip file header is wrong!");     //ошибка начала файла
                    return;
                }
            }

            for (int pos = 0; pos < rPart.array.Length; pos++)
            {
                if (isPrefixFound)
                    block.Add(rPart.array[pos]);

                if (_gzip.FindNewPrefix(ref prefixBuffer, rPart.array[pos]) ||    //при нахождении следующего заголовка
                    rPart.indx == state.TotalParts - 1 && pos == rPart.array.Length - 1)     //или достижении последней группы
                {
                    if (block.Count == 0)
                    {
                        block.AddRange(_gzip.Prefix);
                        isPrefixFound = true;
                    }
                    else
                    {
                        block.RemoveRange(block.Count - _gzip.Prefix.Length, _gzip.Prefix.Length);  //стираем заголовок нового блока

                        using (MemoryStream mStream = new MemoryStream(block.ToArray()))
                        {
                            int bytesRead;  //количество счиатнных байт
                            byte[] decompressed = new byte[state.BlockSize];     //массив для распакованных данных

                            using (GZipStream gzipStream = new GZipStream(mStream, CompressionMode.Decompress))
                            {
                                bytesRead = gzipStream.Read(decompressed, 0, decompressed.Length);
                            }

                            zPart.AddRange(decompressed.Take(bytesRead).ToArray());     //записываем распакованный блок
                        }

                        block.RemoveRange(_gzip.Prefix.Length, block.Count - _gzip.Prefix.Length);  //стираем все, кроме заголовка

                        if (pos - _gzip.Prefix.Length > state.PartLength)     //при достижении границ группы
                            break;
                    }
                }
            }
        }        

        //Запись обработанных блоков в выходной файл (zQueue -> trgFile)
        public void WriteToFile (MultiThreadState state)
        {
            using (FileStream toStream = new FileStream(state.trgFile.FullName, FileMode.OpenOrCreate, FileAccess.Write))
            {
                byte[] array;
                while (!state.isAllDataHandle(ThreadFunc.WRITE))   //пока не записаны все части
                {
                    state.WriteThreadNeed.WaitOne();   //ждем обработанной группы блоков
                    while (state.GetZipPart(out array))        //получаем следующую группу для записи
                        toStream.Write(array, 0, array.Count());
                }
                state.Complete();
            }
        }

    }
}
