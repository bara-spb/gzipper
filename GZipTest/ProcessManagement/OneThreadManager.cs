﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace GZipper
{
    public class OneThreadManager : ProcessManager
    {
        public OneThreadManager (): base() { }
        
        //Запуск процесса обработки
        public override ProcessResult StartProcess (ProcessInitData initData, Action<double> progressCallback)
        {
            using (OneThreadState state = new OneThreadState(initData, this))
            {
                try
                {
                    Thread t;
                    if (initData.Mode == ProcessMode.Compress)
                        t = new Thread(() => CompressInOneThread(state));
                    else
                        t = new Thread(() => DecompressInOneThread(state));
                    t.IsBackground = true;
                    t.Start();
                    
                    while (state.State == ProcessStates.InWork)     //ожидание завершения процесса обработки
                    {
                        progressCallback?.Invoke(state.Progress);   //отображение прогресса выполнения
                        Thread.Sleep(200);
                    }
                }
                catch (Exception ex)
                {
                    state.SetError(ex.Message);
                }
                return new ProcessResult(state.State == ProcessStates.Succeed, state.ErrorMsg);
            }
        }

        //Сжатие в одном потоке
        public void CompressInOneThread (OneThreadState state)
        {
            using (FileStream srcStream = new FileStream(state.srcFile.FullName, FileMode.Open, FileAccess.Read, FileShare.Read, state.BlockSize))
            {
                using (FileStream trgStream = new FileStream(state.trgFile.FullName, FileMode.Create, FileAccess.Write))
                {
                    byte[] buffer = new byte[state.BlockSize];
                    int readBytes;
                                        
                    while ((readBytes = srcStream.Read(buffer, 0, buffer.Length)) > 0)      //считываем блок
                    {                                                
                        if (readBytes != buffer.Length && srcStream.Position != srcStream.Length)
                        {
                            state.SetError("Error ocured while reading the file!");     //ошибка чтения
                            return;
                        }

                        using (MemoryStream mStream = new MemoryStream())
                        {
                            using (GZipStream gzipStream = new GZipStream(mStream, CompressionMode.Compress, true))
                            {                                
                                gzipStream.Write(buffer, 0, readBytes);     //сжимаем блок в буффер
                            }

                            state.SetCurrentPos(srcStream.Position);      //обновляем количество считанных байтов
                                                        
                            int length = (int) mStream.Length;
                            trgStream.Write(mStream.ToArray(), 0, length);  //записываем сжатый блок в файл
                        }
                    }
                    state.Complete();
                }
            }
        }

        //Декомпрессия в одном потоке
        public void DecompressInOneThread (OneThreadState state)
        {
            using (FileStream srcStream = new FileStream(state.srcFile.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (FileStream trgStream = new FileStream(state.trgFile.FullName, FileMode.Create, FileAccess.Write))
                {                    
                    byte[] filePrefix = new byte[_gzip.Prefix.Length];  //буффер для поиска заголовка
                    srcStream.Read(filePrefix, 0, filePrefix.Length);

                    if (!_gzip.IsGZipPrefix(filePrefix))      //проверка первого блока на наличие заголовка
                    {                        
                        state.SetError("Gzip file header is wrong!");       //ошибка начала файла
                        return;
                    }

                    List<byte> prefixBuffer = new List<byte>();     //структура для поиска начала блока
                    List<byte> block = new List<byte>();        //массив байтов блока
                    block.AddRange(_gzip.Prefix);         //добавляем в буфер заголовок

                    int bytesRead;  //количество считанных байт
                    int result;     //результат побайтового чтения
                    byte[] decompressed = new byte[state.BlockSize];   //буффер для распакованного блока

                    while ((result = srcStream.ReadByte()) >= 0)
                    {
                        byte nextByte = (byte) result;
                        block.Add(nextByte);    //добавляем считанный байт к буфферу
                                                
                        if (_gzip.FindNewPrefix(ref prefixBuffer, nextByte))        //при обнаружении начала следующего блока
                        {
                            block.RemoveRange(block.Count - _gzip.Prefix.Length, _gzip.Prefix.Length);      //удаляем считанный префикс из текущего блока

                            using (MemoryStream mStream = new MemoryStream(block.ToArray()))
                            {
                                using (GZipStream gzipStream = new GZipStream(mStream, CompressionMode.Decompress))
                                {
                                    bytesRead = gzipStream.Read(decompressed, 0, decompressed.Length);  //производим декомпрессию блока в буфер
                                }
                            }

                            state.SetCurrentPos(srcStream.Position);       //обновляем счетчик записанных блоков (для расчета прогресса декомпрессии)
                            trgStream.Write(decompressed, 0, bytesRead);    //записываем распакованный блок в файл
                            block.Clear();              //обнуляем буффер блока
                            block.AddRange(_gzip.Prefix);     //и добавляем к нему ранее считанный заголовок
                        }
                    }
                                        
                    if (srcStream.Position == srcStream.Length && block.Count > prefixBuffer.Count)     //по достижении конца файла
                    {
                        using (MemoryStream mStream = new MemoryStream(block.GetRange(0, block.Count - prefixBuffer.Count).ToArray()))
                        {
                            using (GZipStream gzipStream = new GZipStream(mStream, CompressionMode.Decompress))
                            {
                                bytesRead = gzipStream.Read(decompressed, 0, decompressed.Length);      //записываем последний блок
                            }
                        }
                        trgStream.Write(decompressed, 0, bytesRead);
                        state.Complete();
                    }
                    else
                    {
                        state.SetError("Error ocured while decompessing. Unexpected file ending.");        //неожиданное окончание файла
                        return;
                    }
                }
            }
        }
    }
}
