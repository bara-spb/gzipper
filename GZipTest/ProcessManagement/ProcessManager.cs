﻿using GZipper.Utils;
using System;
using System.Threading;

namespace GZipper
{
    abstract public class ProcessManager : IProcessManager
    {
        protected GZipUtils _gzip = new GZipUtils();  //вспомогательные функции по поиску заголовка блоков GZip архива

        public ProcessManager () { }

        //Запуск процесса обработки
        abstract public ProcessResult StartProcess (ProcessInitData initData, Action<double> progressCallback);        
    }
}
