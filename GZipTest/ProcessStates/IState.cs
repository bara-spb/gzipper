﻿using System;

namespace GZipper
{
    public interface IState : IDisposable
    {
        ProcessStates State { get; }    //состояние процесса обработки
        ProcessMode Mode { get; }       //режим обработки [сжатие/декомпрессия]
        string ErrorMsg { get; }        //сообщение об ошибке
        double Progress { get; }        //прогресс выполнения операции
        void Complete ();               //успешное завершение операции
        void SetError (string msg);     //аварийное завершение операции
    }
}