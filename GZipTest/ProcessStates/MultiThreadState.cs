﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.VisualBasic.Devices;
using static GZipper.MultiThreadManager;

namespace GZipper
{
    public sealed class MultiThreadState : StateBase
    {
        #region variables
        
        //-------------------Синхронизация--------------------------------------------

        private object _readLocker = new object();      //объект для синхронизации доступа к очереди считанных блоков
        private object _zipLocker = new object();       //объект для синхронизации доступа к очереди обработанных блоков

        public AutoResetEvent ReadThreadNeed = new AutoResetEvent(false);       //запрос на создание потока чтения
        public AutoResetEvent ZipThreadNeed = new AutoResetEvent(false);        //запрос на создание потока обработки
        public AutoResetEvent WriteThreadNeed = new AutoResetEvent(false);        //запрос на создание потока записи

        //-------------------Счетчики-------------------------------------------------

        public readonly int TotalParts;     //общее количество групп блоков
        public readonly int PartCapacity;   //количество блоков в группе
        public readonly int PartLength;     //размер группы в байтах

        public int ReadIndx { get; private set; } = 0;        //индекс считываемой группы блоков
        public int ZipIndx { get; private set; } = 0;         //индекс обрабатываемой группы блоков
        public int WriteIndx { get; private set; } = 0;       //индекс записываемой группы блоков

        //-------------------Контроль памяти и скорости выполнения--------------------
        
        public ProcessEvolutionControl procEvolution { get; private set; }    //контроллер выполнения процесса обработки
        public bool LackOfMemory { get; set; } = false;     //флаг нехватки памяти
        //public bool manualOutOfMemory = false;      //TODO delete - manual OutOfMemory set

        //-------------------Буфферы групп блоков-------------------------------------

        private Queue<BlockPart> _rQueue = new Queue<BlockPart>();     //список считанных групп блоков
        private Dictionary<int, byte[]> _zQueue = new Dictionary<int, byte[]>();   //список обработанных групп блоков

        #endregion
        
        public MultiThreadState (ProcessInitData initData) : base(initData)
        {
            TotalParts = (int) Math.Ceiling((double) srcFile.Length / (initData.PartCapacity * BlockSize));
            PartCapacity = initData.PartCapacity;
            PartLength = PartCapacity * initData.BlockSize;

            procEvolution = new ProcessEvolutionControl(this, SetError);

            //StartManualOutOfMemorySwitcher();       //TODO delete - manual OutOfMemory
        }

        //Прогресс выполнения операции
        protected override double GetProgress ()
        {
            return (double) WriteIndx / TotalParts;
        }


        //Все ли данные были отобраны для конкретного типа операции
        public bool isAllDataHandle (ThreadFunc spec)
        {
            if (spec == ThreadFunc.READ)
                return ReadIndx >= TotalParts;
            if (spec == ThreadFunc.ZIP)
                return ZipIndx >= TotalParts;
            if (spec == ThreadFunc.WRITE)
                return WriteIndx >= TotalParts;
            return true;
        }

        //Индекс следующей группы для считывания
        public bool GetNextReadIndx (ref int indx)
        {
            if (ReadIndx >= TotalParts)
                return false;
            indx = ReadIndx++;
            return true;
        }

        //Добавление группы считанных блоков в очередь для обработки
        public bool AddReadPart (int indx, byte[] array)
        {
            //если заканчивается доступная память и еще имеются 
            //считанные блоки для обработки или обработанные блоки для записи - приостанавливаем чтение
            if (LackOfMemory && (_rQueue.Count != 0 || _zQueue.ContainsKey(WriteIndx)))
                return false;

            try
            {
                lock (_readLocker)
                {
                    //TODO delete - manual OutOfMemory
                    //if (manualOutOfMemory)
                    //    throw new OutOfMemoryException();

                    _rQueue.Enqueue(new BlockPart(indx, array));
                    ZipThreadNeed.Set();
                }
                return true;
            }
            catch (OutOfMemoryException e)
            {
                GC.Collect();
                return false;
            }
        }

        //Извлечение группы считанных блоков для обработки
        public bool GetReadPart (out BlockPart part)
        {
            lock (_readLocker)
            {
                if (_rQueue.Count > 0)
                {
                    ZipIndx++;
                    part = _rQueue.Dequeue();
                    return true;
                }
            }
            ReadThreadNeed.Set();
            part = new BlockPart();
            return false;
        }

        //Добавление группы обработанных блоков в очередь для записи в файл
        public bool AddZipPart (int indx, byte[] array)
        {
            //если заканчивается доступная память и еще имеются 
            //обработанные блоки для записи - приостанавливаем запись
            if (LackOfMemory && _zQueue.ContainsKey(WriteIndx))
                return false;

            try
            {
                lock (_zipLocker)
                {
                    //TODO delete - manual OutOfMemory
                    //if (manualOutOfMemory)
                    //    throw new OutOfMemoryException();

                    _zQueue.Add(indx, array);
                    WriteThreadNeed.Set();
                    return true;
                }
            }
            catch (OutOfMemoryException e)
            {
                GC.Collect();
                return false;
            }
        }

        //Извлечение группы обработанных блоков для записи в файл
        public bool GetZipPart (out byte[] array)
        {
            lock (_zipLocker)
            {
                if (_zQueue.ContainsKey(WriteIndx))   //если блок уже обработан - формируем данные
                {
                    array = _zQueue[WriteIndx];
                    _zQueue.Remove(WriteIndx++);
                    return true;
                }
            }
            ZipThreadNeed.Set();
            array = null;
            return false;
        }

        //Освобождение ресурсов
        public override void Dispose ()
        {
            procEvolution?.Dispose();
            ReadThreadNeed?.Close();
            ZipThreadNeed?.Close();
            WriteThreadNeed?.Close();
            base.Dispose();
        }

        //TODO delete - manual OutOfMemory
        //private void StartManualOutOfMemorySwitcher ()
        //{
        //    Thread mThread = new Thread(() =>
        //    {
        //        while (true)
        //        {
        //            ConsoleKey key = Console.ReadKey().Key;
        //            if (key == ConsoleKey.B)
        //                manualOutOfMemory = true;
        //            else if (key == ConsoleKey.R)
        //                manualOutOfMemory = false;
        //        }
        //    });
        //    mThread.IsBackground = true;
        //    mThread.Start();
        //}
    }
}
