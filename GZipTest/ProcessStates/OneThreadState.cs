﻿using System;

namespace GZipper
{
    public sealed class OneThreadState : StateBase
    {
        public long ReadPos { get; private set; } = 0;   //количество считанных байтов (для расчета прогресса выполнения)

        public OneThreadState(ProcessInitData initData, OneThreadManager manager) : base(initData) { }

        //Прогресс выполнения операции
        protected override double GetProgress()
        {
            return (double) ReadPos / srcFile.Length;
        }

        //Обновления количества считанных байтов
        public void SetCurrentPos (long pos)
        {
            ReadPos = Math.Max(ReadPos, pos);
        }

    }
}
