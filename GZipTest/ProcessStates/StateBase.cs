﻿using GZipper.Utils;
using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace GZipper
{
    public enum ProcessStates { InWork, Succeed, Error }    //состояния процесса обрабтки
    public enum ProcessMode { Compress, Decompress }        //режим обработки

    abstract public class StateBase : IState
    {
        public ProcessMode Mode { get; protected set; }     //режим обработки [сжатие/декомпрессия]
        public ProcessStates State { get; protected set; }  //состояние процесса обработки
        public string ErrorMsg { get; protected set; }      //сообщение об ошибке

        public double Progress { get { return GetProgress(); } }    //прогресс выполнения операции

        public FileInfo srcFile { get; private set; }   //исходный файл
        public FileInfo trgFile { get; private set; }   //выходной файл  

        public int BlockSize { get; private set; }      //размер блока данных
        
        public StateBase (ProcessInitData initData)
        {
            State = ProcessStates.InWork;
            Mode = initData.Mode;

            srcFile = initData.SourceFile;
            trgFile = initData.TargetFile;
            BlockSize = initData.BlockSize;
        }

        //Аварийное завершение операции
        public void SetError (string msg)
        {
            ErrorMsg = msg;
            State = ProcessStates.Error;
        }

        //Успешное завершение операции
        public void Complete ()
        {
            State = ProcessStates.Succeed;
        }

        //Прогресс выполнения операции
        abstract protected double GetProgress ();

        //Освобождение ресурсов
        public virtual void Dispose () { }
    }
}
