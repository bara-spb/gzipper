﻿using System;

namespace GZipper
{
    class Program
    {
        static void Main (string[] args)
        {
            GZipper gzip = new GZipper();
            gzip.Execute(args);
            
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
