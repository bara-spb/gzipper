﻿using System.Collections.Generic;

namespace GZipper
{
    //Структура для хранения считанной или обработанной группы блоков
    public struct BlockPart
    {
        public int indx;    //индекс группы
        public byte[] array;     //массив данных

        public BlockPart (int indx, byte[] array)
        {
            this.indx = indx;
            this.array = array;
        }
    }    
}
