﻿using System.IO;
using static GZipper.StateBase;

namespace GZipper
{
    //Обертка для исходных данных процесса обработки
    public struct ProcessInitData
    {
        public ProcessMode Mode;        //режим обработки
        public FileInfo SourceFile;     //исходный файл
        public FileInfo TargetFile;     //выходной файл
        public int BlockSize;           //размер блока
        public int PartCapacity;        //количество блоков в группе
        
        public ProcessInitData (FileInfo src, FileInfo trg, string cmd, int blockSize = 1024*1024, int partCapacity = 3) : 
            this(src, trg, cmd == GZipper.cmdCompress ? ProcessMode.Compress : ProcessMode.Decompress, blockSize, partCapacity) { }

        public ProcessInitData (FileInfo src, FileInfo trg, ProcessMode mode, int blockSize = 1024*1024, int partCapacity = 3)
        {
            Mode = mode;
            SourceFile = src;
            TargetFile = trg;
            BlockSize = blockSize;
            PartCapacity = partCapacity;
        }

    }
}
