﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GZipper
{
    //Результат выполнения процесса обработки
    public struct ProcessResult
    {
        public bool Success;    //результат выполнения операции
        public string ErrorMsg;     //сообщение об ошибке

        public ProcessResult (bool success, string err)
        {
            Success = success;
            ErrorMsg = err;
        }
    }
}
