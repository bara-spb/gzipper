﻿using System;
using System.Collections.Generic;

namespace GZipper.Utils
{
    public class GZipUtils
    {
        public readonly byte[] Prefix = new byte[] { 31, 139, 8, 0 };      //начало заголовка блока GZip

        //Является ли переданный массив заголовком блока
        public bool IsGZipPrefix (byte[] array)
        {
            if (array.Length != Prefix.Length)
                return false;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != Prefix[i])
                    return false;
            }
            return true;
        }

        //Поиск нового заголовка блока путем последовательного считывания байтов
        public bool FindNewPrefix (ref List<byte> prefixBuffer, byte lastByte)
        {
            if (Prefix[prefixBuffer.Count] == lastByte)
            {
                prefixBuffer.Add(lastByte);
                if (prefixBuffer.Count == Prefix.Length)
                {
                    prefixBuffer.Clear();
                    return true;
                }
            }
            else
                prefixBuffer.Clear();
            return false;
        }        

    }
}
