﻿using Microsoft.VisualBasic.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using static GZipper.MultiThreadManager;

namespace GZipper
{
    //Контроль доступной памяти и скорости выполнения процесса обработки
    public class ProcessEvolutionControl : IDisposable
    {
        private MultiThreadState _state;      //ссылка на объект состояния процесса обработки

        private int _readCountOld;        //количество считанных блоков на предыдущей итерации
        private int _writeCountOld;       //количество записанных блоков на предыдущей итерации
        private int _zipCountOld;         //количество обработанных блоков на предыдущей итерации

        private Dictionary<ThreadFunc, int> _deltas;    //скорость выполнения операций по назначению за период

        private Timer _timer;
        private DateTime _downtimeStart = DateTime.MaxValue;    //время начала простоя
        private int _downtimeMsLimit = 5000;        //максимальное время простоя в мс
        private int counter = 0;
        
        private readonly ComputerInfo _compInfo = new ComputerInfo();
        private readonly ulong _minMemoryLimit;     //критическое значение доступной памяти

        public ProcessEvolutionControl(MultiThreadState state, Action<string> downtimeHandler)
        {
            _state = state;

            _readCountOld = state.ReadIndx;
            _zipCountOld = state.ZipIndx;
            _writeCountOld = state.WriteIndx;

            _deltas = new Dictionary<ThreadFunc, int>() { { ThreadFunc.READ, 0 }, { ThreadFunc.ZIP, 0 }, { ThreadFunc.WRITE, 0 } };

            _minMemoryLimit = (ulong) (_state.PartLength * 10);

            _timer = new Timer(100);
            _timer.Enabled = true;     //TODO turn on
            _timer.Elapsed += (o, e) => OnTimerElapsed(downtimeHandler);
        }

        //Контроль хода выполнения процесса обработки, его завершение при длительном простое
        private void OnTimerElapsed(Action<string> downtimeHandler)
        {
            _state.LackOfMemory = _compInfo.AvailableVirtualMemory < _minMemoryLimit;       //регулярно обновляем состояние памяти

            if (counter % 10 != 0)      //контроль скорости выполнения операций выполняем в 10 раз реже
                return;

            UpdateState();  //обновляем показатели

            if (_state.ReadIndx >= 0 && !_state.isAllDataHandle(ThreadFunc.WRITE) && IsZeroDelta())   //если процесс начат, но скорость выполнения всех операций равно нулю
            {
                if (_downtimeStart == DateTime.MaxValue)        //при первом вхождении - засекаем время начала простоя
                    _downtimeStart = DateTime.Now;
                else if ((DateTime.Now - _downtimeStart).TotalMilliseconds > _downtimeMsLimit)   //если время превысило максимально допустимое - завершаем процесс с ошибкой
                {
                    downtimeHandler("Process was stopped due to downtime limit excess");
                    this.Dispose();
                }
            }
            else
                _downtimeStart = DateTime.MaxValue;     //сбрасываем время начала простоя
        }

        //Обновление показателей скорости выполнения операций
        private void UpdateState ()
        {
            _deltas[ThreadFunc.READ] = _state.ReadIndx - _readCountOld;
            _deltas[ThreadFunc.WRITE] = _state.WriteIndx - _writeCountOld;
            _deltas[ThreadFunc.ZIP] = _state.ZipIndx - _zipCountOld;

            _readCountOld = _state.ReadIndx;
            _zipCountOld = _state.ZipIndx;
            _writeCountOld = _state.WriteIndx;
        }
        
        private bool IsZeroDelta()
        {
            return _deltas.Values.ToArray().All(d => { return d == 0; });
        }

        public void Dispose ()
        {
            _timer?.Dispose();
        }

        //TODO - can delete (Используется для сбора статистики по времени выполнения отдельных операций)
        public void ResetTimer ()
        {
            _timer.Stop();
        }

        //TODO - can delete (Используется для сбора статистики по времени выполнения отдельных операций)
        public void RestartTimer ()
        {
            _timer.Start();
        }

    }
}
