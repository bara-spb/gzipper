﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using static GZipper.MultiThreadManager;
using static GZipper.StateBase;

namespace GZipper.Statistics
{
    partial class StatCollector
    {
        public void CompressInOneThread (List<byte[]> rBlocks, ref List<byte[]> zBlocks, ProcessInitData initData)
        {
            foreach (byte[] block in rBlocks)
            {
                if (initData.Mode == ProcessMode.Compress)
                {
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        using (GZipStream gzipStream = new GZipStream(mStream, CompressionMode.Compress, true))
                        {
                            gzipStream.Write(block, 0, block.Length);
                        }
                        zBlocks.Add(mStream.ToArray());
                    }
                }
                else
                {
                    byte[] decompressed = new byte[initData.BlockSize];   //буффер для распакованного блока
                    int bytesRead;

                    using (MemoryStream mStream = new MemoryStream(block.ToArray()))
                    {
                        using (GZipStream gzipStream = new GZipStream(mStream, CompressionMode.Decompress))
                        {
                            bytesRead = gzipStream.Read(decompressed, 0, decompressed.Length);  //производим декомпрессию блока в буфер
                        }

                        if (bytesRead == initData.BlockSize)
                            zBlocks.Add(decompressed);
                        else
                            zBlocks.Add(decompressed.Take(bytesRead).ToArray());
                    }

                }
            }
        }

        public void CompressInThreeThread (MultiThreadManager manager, MultiThreadState state)
        {
            List<Thread> threads = new List<Thread>();
            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                Thread t = (new Thread(() => manager.ZipToBuffer(state)));
                t.IsBackground = true;
                t.Start();
                threads.Add(t);
            }
            for (int i = 0; i < threads.Count; i++)
                state.ZipThreadNeed.Set();      //пробуждаем все потоки

            while (!state.isAllDataHandle(ThreadFunc.ZIP)) { Thread.Sleep(100); }

            for (int i = 0; i < threads.Count; i++)
                state.ZipThreadNeed.Set();      //пробуждаем все потоки

            threads.ForEach(t => t.Join());
        }    
    }
}
