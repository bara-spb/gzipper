﻿using GZipper.Tests;
using System.IO;
using System;
using static GZipper.StateBase;

namespace GZipper.Statistics
{
    partial class StatCollector
    {
        public bool FullCompress (CheckFileInfo info, ProcessImplementation impl, ProcessMode mode)
        {
            ProcessInitData initData = new ProcessInitData(info.GetSourceFile(mode), info.GetTargetFile(mode), mode);
            
            ProcessResult result;

            if (impl == ProcessImplementation.OneThread)
                result = new OneThreadManager().StartProcess(initData, null);
            else if (impl == ProcessImplementation.AllDividedThreads)
                result = new MultiThreadManager().StartProcess(initData, null);

            FileInfo trgFile = info.GetTargetFile(mode);
            return trgFile.Length == info.GetTargetSize(mode);
        }
    }
}
