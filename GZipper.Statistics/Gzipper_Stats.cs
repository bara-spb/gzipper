﻿using System;
using System.Diagnostics;
using GZipper.Tests;
using static GZipper.Statistics.StatsUtils;
using System.Collections.Generic;
using static GZipper.StateBase;
using System.IO;
using System.Threading;

namespace GZipper.Statistics
{
    struct processInfo
    {
        public bool success;
        public long timeElapsed;

        public processInfo(bool success, long time)
        {
            this.success = success;
            timeElapsed = time;
        }
    }

    public enum ProcessImplementation { OneThread, AllDividedThreads }

    public class Statistics
    {
        static void Main (string[] args)
        {
            int procCount = Environment.ProcessorCount;

            Console.WriteLine("Start collecting statistics ...");

            string title = string.Format("Comparing operation times, processor counts = {0}", procCount);
            string[] headers = new[] { "Operation", "OneThread", "MultiThread" };
            StatTable table = new StatTable(title, headers, 80);

            StatCollector statCollector = new StatCollector();

            Dictionary<ProcessImplementation, bool> testSwitcher = new Dictionary<ProcessImplementation, bool>()
            {
                { ProcessImplementation.OneThread, true },
                { ProcessImplementation.AllDividedThreads, true }
            };

            foreach (CheckFileInfo file in TestData.TestFiles)
            {
                if(!File.Exists(file.OriginName))
                {
                    Console.SetCursorPosition(0, Console.CursorTop);
                    Console.WriteLine(string.Format("Can't find file \"{0}\". Put test files in testDir in root of solution and add them to TestFiles list in TestData class", file.OriginName), false);
                    table.CloseRow();
                    continue;
                }

                string fileHeader = string.Format("File \"{1}\", size = {0}", GetFileSize(file.OriginSize), file.GetSourceFile(ProcessMode.Compress).Name);

                table.AddSubheader(fileHeader, false);

                List<byte[]> rBlocks = new List<byte[]>();
                List<byte[]> zBlocks = new List<byte[]>();
                
                ProcessInitData initData = new ProcessInitData(file.GetSourceFile(ProcessMode.Compress), file.GetTargetFile(ProcessMode.Compress), ProcessMode.Compress);

                MultiThreadManager mtManager = new MultiThreadManager();

                using (MultiThreadState mtState = new MultiThreadState(initData))
                {
                    mtState.procEvolution.ResetTimer();

                    table.AddSubheader("Compression:", false, false);

                    table.AddRowData("Read", 0, true);
                    if (testSwitcher[ProcessImplementation.OneThread])
                        table.AddRowData(MeasureTime(() => statCollector.ReadForCompressInOneThread(ref rBlocks, file)), 1);
                    if (testSwitcher[ProcessImplementation.AllDividedThreads])
                        table.AddRowData(MeasureTime(() => statCollector.ReadInDividedThread(mtManager, mtState)), 2);
                    table.CloseRow();

                    table.AddRowData("Compress", 0, true);
                    if (testSwitcher[ProcessImplementation.OneThread])
                        table.AddRowData(MeasureTime(() => statCollector.CompressInOneThread(rBlocks, ref zBlocks, initData)), 1);
                    if (testSwitcher[ProcessImplementation.AllDividedThreads])
                        table.AddRowData(MeasureTime(() => statCollector.CompressInThreeThread(mtManager, mtState)), 2);
                    table.CloseRow();

                    table.AddRowData("Write", 0, true);
                    if (testSwitcher[ProcessImplementation.OneThread])
                        table.AddRowData(MeasureTime(() => statCollector.WriteInOneThread(zBlocks, file, ProcessMode.Compress)), 1);
                    if (testSwitcher[ProcessImplementation.AllDividedThreads])
                        table.AddRowData(MeasureTime(() => statCollector.WriteInThreeThread(mtManager, mtState)), 2);
                    table.CloseRow();

                    Thread.Sleep(300);  //ожидание закрытия файла

                    table.AddRowData("Full", 0, true);
                    if (testSwitcher[ProcessImplementation.OneThread])
                        table.AddRowData(MeasureTime(() => statCollector.FullCompress(file, ProcessImplementation.OneThread, initData.Mode)), 1);
                    if (testSwitcher[ProcessImplementation.AllDividedThreads])
                        table.AddRowData(MeasureTime(() => statCollector.FullCompress(file, ProcessImplementation.AllDividedThreads, initData.Mode)), 2);
                    table.CloseRow();
                }

                initData = new ProcessInitData(file.GetSourceFile(ProcessMode.Decompress), file.GetTargetFile(ProcessMode.Decompress), ProcessMode.Decompress);
                rBlocks.Clear();
                zBlocks.Clear();
                Thread.Sleep(100);

                using (MultiThreadState mtState = new MultiThreadState(initData))
                {
                    mtState.procEvolution.ResetTimer();

                    table.AddEmpryRow();
                    table.AddSubheader("Decompression:", false, false);

                    table.AddRowData("Read", 0, true);
                    if (testSwitcher[ProcessImplementation.OneThread])
                        table.AddRowData(MeasureTime(() => statCollector.ReadForDecompressInOneThread(ref rBlocks, file, initData.BlockSize)), 1);
                    if (testSwitcher[ProcessImplementation.AllDividedThreads])
                        table.AddRowData(MeasureTime(() => statCollector.ReadInDividedThread(mtManager, mtState)), 2);
                    table.CloseRow();

                    table.AddRowData("Decompress", 0, true);
                    if (testSwitcher[ProcessImplementation.OneThread])
                        table.AddRowData(MeasureTime(() => statCollector.CompressInOneThread(rBlocks, ref zBlocks, initData)), 1);
                    if (testSwitcher[ProcessImplementation.AllDividedThreads])
                        table.AddRowData(MeasureTime(() => statCollector.CompressInThreeThread(mtManager, mtState)), 2);
                    table.CloseRow();

                    table.AddRowData("Write", 0, true);
                    if (testSwitcher[ProcessImplementation.OneThread])
                        table.AddRowData(MeasureTime(() => statCollector.WriteInOneThread(zBlocks, file, ProcessMode.Decompress)), 1);
                    if (testSwitcher[ProcessImplementation.AllDividedThreads])
                        table.AddRowData(MeasureTime(() => statCollector.WriteInThreeThread(mtManager, mtState)), 2);
                    table.CloseRow();

                    Thread.Sleep(300);  //ожидание закрытия файла

                    table.AddRowData("Full", 0, true);
                    if (testSwitcher[ProcessImplementation.OneThread])
                        table.AddRowData(MeasureTime(() => statCollector.FullCompress(file, ProcessImplementation.OneThread, initData.Mode)), 1);
                    if (testSwitcher[ProcessImplementation.AllDividedThreads])
                        table.AddRowData(MeasureTime(() => statCollector.FullCompress(file, ProcessImplementation.AllDividedThreads, initData.Mode)), 2);
                    table.CloseRow();
                }

            }
            table.CloseTable();

            Console.ReadKey();
        }

        static string MeasureTime(Action method)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            method.Invoke();
            sw.Stop();
            return string.Format("{0} ms", sw.ElapsedMilliseconds);
        }

    }
}
