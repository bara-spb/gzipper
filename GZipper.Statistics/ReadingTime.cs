﻿using GZipper.Tests;
using GZipper.Utils;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using static GZipper.MultiThreadManager;

namespace GZipper.Statistics
{
    partial class StatCollector
    {
        public void ReadForCompressInOneThread (ref List<byte[]> rBlocks, CheckFileInfo info)
        {            
            byte[] buffer = new byte[1024*1024];
            int readBytes;

            using (FileStream srcStream = new FileStream(info.OriginName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                while ((readBytes = srcStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    //Ошибка чтения
                    if (readBytes != buffer.Length && srcStream.Position != srcStream.Length)
                        break;

                    if (readBytes == buffer.Length)
                        rBlocks.Add(buffer.ToArray());
                    else
                        rBlocks.Add(buffer.Take(readBytes).ToArray());
                }
            }
        }

        public void ReadForDecompressInOneThread (ref List<byte[]> rBlocks, CheckFileInfo info, int blockSize)
        {
            GZipUtils _gzip = new GZipUtils();
            
            using (FileStream srcStream = new FileStream(info.CompressedName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                byte[] filePrefix = new byte[_gzip.Prefix.Length];  //буффер для заголовка
                srcStream.Read(filePrefix, 0, filePrefix.Length);

                if (!_gzip.IsGZipPrefix(filePrefix))      //проверка первого блока на наличие заголовка
                    return;

                List<byte> prefixBuffer = new List<byte>();     //структура для поиска начала блока
                List<byte> block = new List<byte>();        //массив байтов блока
                block.AddRange(_gzip.Prefix);         //добавляем в буфер заголовок
                
                int result;     //результат побайтового чтения
                byte[] decompressed = new byte[blockSize];   //буффер для распакованного блока

                while ((result = srcStream.ReadByte()) >= 0)
                {
                    byte nextByte = (byte) result;
                    block.Add(nextByte);    //добавляем считанный байт к буфферу

                    if (_gzip.FindNewPrefix(ref prefixBuffer, nextByte))        //при обнаружении начала следующего блока
                    {

                        block.RemoveRange(block.Count - _gzip.Prefix.Length, _gzip.Prefix.Length);      //удаляем считанный префикс из текущего блока
                        
                        rBlocks.Add(block.ToArray());

                        block.Clear();              //обнуляем буффер блока
                        block.AddRange(_gzip.Prefix);     //и добавляем к нему ранее считанный заголовок
                    }
                }

                if (srcStream.Position == srcStream.Length && block.Count > prefixBuffer.Count)     //по достижении конца файла
                    rBlocks.Add(block.ToArray());       //записываем последний блок
            }
        }

        public void ReadInDividedThread (MultiThreadManager manager, MultiThreadState state)
        {
            Thread t = new Thread(() => manager.ReadFromFile(state));
            t.IsBackground = true;
            t.Start();
            t.Join();
        }
    }
}
