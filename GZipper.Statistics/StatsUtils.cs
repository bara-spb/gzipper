﻿using System;

namespace GZipper.Statistics
{
    static class StatsUtils
    {
        public class StatTable
        {
            readonly int rowCount;
            readonly int tableWidth;
            int[] place;
            readonly int xStart;
            readonly int xEnd;
            int yPosOffset;
            int rowIndx = 0;

            public StatTable (string title, string[] headers, int width)
            {
                rowCount = headers.Length;
                xStart = 2;
                int maxWidth = Console.WindowWidth - 2*xStart - 1;
                tableWidth = Math.Min(width, maxWidth);
                xEnd = xStart + tableWidth;
                place = new int[rowCount];

                for (int i = 0; i < rowCount; i++)
                    place[i] = (int)(xStart + (i + 0.5) * tableWidth / rowCount);

                yPosOffset = Console.CursorTop + 1;                

                WriteHorizontalLine();

                if (title.Length < tableWidth)
                {
                    WriteOnPos("|", xStart, yPosOffset);
                    WriteOnCenter(title, tableWidth / 2, yPosOffset);
                    WriteOnPos("|", xEnd, yPosOffset++);
                }
                else
                {
                    string[] titleParts = title.Split(' ');
                    int partsCount = (int) Math.Ceiling((double) title.Length / tableWidth);
                    int partLength = title.Length / partsCount;
                    int endIndx = 0;
                    for (int i = 0; i < partsCount; i++)
                    {
                        int startIndx = endIndx;
                        endIndx = title.IndexOf(' ', startIndx + partLength);
                        string titlePart;
                        if (endIndx > 0)
                            titlePart = title.Substring(startIndx, endIndx - startIndx);
                        else
                            titlePart = title.Substring(startIndx);

                        WriteOnPos("|", xStart, yPosOffset);
                        WriteOnCenter(titlePart, tableWidth / 2, yPosOffset);
                        WriteOnPos("|", xEnd, yPosOffset++);
                    }
                }
                WriteHorizontalLine();


                WriteOnPos("|", xStart, yPosOffset);
                for (int i = 0; i < rowCount; i++)
                    WriteOnCenter(headers[i], place[i], yPosOffset);
                WriteOnPos("|", xEnd, yPosOffset++);
            }

            public void AddNewRowIndx ()
            {
                WriteOnPos("|", xStart, yPosOffset);
                WriteOnPos(rowIndx++, place[0], yPosOffset);
            }

            public void StartRow (bool fromNewLine = true)
            {
                WriteOnPos("|", xStart, fromNewLine ? yPosOffset++ : yPosOffset);
            }
            public void CloseRow ()
            {
                WriteOnPos("|", xEnd, yPosOffset++);
            }
            public void AddRowData (string data, int row, bool startNewRow = false)
            {
                if(startNewRow)
                    WriteOnPos("|", xStart, yPosOffset);
                WriteOnCenter(data, place[row], yPosOffset);
            }

            public void AddEmpryRow ()
            {
                StartRow(false);
                CloseRow();
            }

            public void AddSubheader (string data, bool closeBelow = true, bool closeAbove = true)
            {
                if(closeAbove)
                    WriteHorizontalLine();
                StartRow(false);
                WriteOnCenter(data, xStart + tableWidth/2, yPosOffset);
                CloseRow();
                if (closeBelow)
                    WriteHorizontalLine();
                else
                {
                    StartRow(false);
                    CloseRow();
                }
            }

            public void AddRowAutoCount (string[] data)
            {
                WriteOnPos("|", xStart, yPosOffset);
                WriteOnPos(rowIndx++, place[0], yPosOffset);

                for (int i = 1; i < rowCount; i++)
                    WriteOnCenter(data[i - 1], place[i], yPosOffset);

                WriteOnPos("|", xEnd, yPosOffset++);
            }

            public void AddRow (string[] data, bool notAlignFirstColumn = false)
            {
                WriteOnPos("|", xStart, yPosOffset);

                for (int i = 0; i < rowCount; i++)
                {
                    if(i == 0 && notAlignFirstColumn)
                        WriteOnPos(data[i], place[i], yPosOffset);
                    else
                        WriteOnCenter(data[i], place[i], yPosOffset);
                }

                WriteOnPos("|", xEnd, yPosOffset++);
            }

            public void CloseTable ()
            {
                WriteHorizontalLine();
            }

            public static void WriteOnPos (string msg, int x, int y)
            {
                Console.SetCursorPosition(x, y);
                Console.Write(msg);
            }

            public static void WriteOnPos (object val, int x, int y)
            {
                Console.SetCursorPosition(Math.Max(2, x), y);
                Console.Write(val.ToString());
            }

            public static void WriteOnCenter (string msg, int x, int y)
            {
                WriteOnPos(msg, Math.Max(2, x - msg.Length / 2), y);
            }

            public static void WriteOnCenter (object val, int x, int y)
            {
                WriteOnCenter(val.ToString(), x, y);
            }

            public void WriteHorizontalLine ()
            {
                WriteOnPos(new String('-', tableWidth+1), xStart, yPosOffset++);
            }

        }

        public static string GetFileSize(long fileSize)
        {
            double size = fileSize;
            string[] abbr = new string[] {"b", "Kb", "Mb", "Gb" };
            for (int i = 0; i < abbr.Length; i++)
            {
                if (size < 1000 || i == abbr.Length - 1)
                    return Math.Round(size, 2) + abbr[i];
                size /= 1024;
            }
            return Math.Round(size, 2) + "Tb";
        }

    }
}
