﻿using GZipper.Tests;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using static GZipper.StateBase;

namespace GZipper.Statistics
{
    partial class StatCollector
    {
        public void WriteInOneThread (List<byte[]> zBlocks, CheckFileInfo file, ProcessMode mode)
        {
            using (FileStream trgStream = new FileStream(file.GetTargetFile(mode).FullName, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                foreach (byte[] block in zBlocks)
                {
                    trgStream.Write(block, 0, block.Length);
                }
            }
        }

        public  void WriteInThreeThread (MultiThreadManager manager, MultiThreadState state)
        {
            state.WriteThreadNeed.Set();
            Thread t = new Thread(() => manager.WriteToFile(state));
            t.IsBackground = true;
            t.Start();
            state.procEvolution.RestartTimer();
            t.Join();
        }
    }
}
